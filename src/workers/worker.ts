import log from "loglevel";

export interface WorkerCallbackProps {
  name: string;
  message: unknown;
}

export const workerCallback = (worker: WorkerCallbackProps): void => {
  log.info(worker.name);
  log.info(worker.message);
};

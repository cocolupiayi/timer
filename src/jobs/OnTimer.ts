import { parentPort } from "node:worker_threads";
import process from "node:process";

console.log("OnTimer Test");

if (parentPort) parentPort.postMessage({data: "done"});
else process.exit(0);

import Bree from "bree";
import express from "express";
import log from "loglevel";
import moment from "moment";
import path from "path";
import BreeTs from "@breejs/ts-worker";
import { workerCallback } from "./workers/worker";
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import swaggerJsdoc from "swagger-jsdoc";
import TimerService from "./service/TimerService";

export const app = express();

const port = 5000;

log.setLevel(1);
app.use(cors());
app.options('*', cors);

app.use("/api-docs",swaggerUi.serve, swaggerUi.setup(swaggerJsdoc(
  {
    definition: {
      openapi: "3.0.0",
      info: {
        "title": "timer api",
        "version": "1.0.0"
      }
    },
    apis: ["./src/**/*.ts","./src/**/*.js"]
  }
)));

Bree.extend(BreeTs);

const bree = new Bree({
  root: path.join(__dirname, "jobs"),
  defaultExtension: "ts",
  jobs: [
    {
      name: "OnTimer",
      date: moment().add(30, "seconds").toDate()
    }
  ],
  workerMessageHandler: workerCallback
});

// log.info(bree);

(async () => {
  // add a job array after initialization:
  await bree.add({
    name: "OffTimer",
    date: moment().add(60, "seconds").toDate()
  }); // will return array of added jobs
  // this must then be started using one of the above methods
  bree.start("OffTimer");
  log.info(bree.config.jobs);
  
  // add a job after initialization:
  // await bree.add('OffTimer');
  // this must then be started using one of the above methods
})();

(async () => {
  // add a job array after initialization:
  await bree.start();
})();

app.listen(port, () => {
  console.log(`🚀 server started at http://localhost:${port}`)
});

app.use("/timer", TimerService);

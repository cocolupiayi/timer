import express from "express";
import log from "loglevel";
import {
  OffTimerDataInterface,
  OnTimerDataInterface,
  TimerModulesType,
} from "../timer/interface/DataInterface";
import { Timer } from "../timer/Timer";

const router = express.Router();

export const timerModules = new Map<string, TimerModulesType>();

timerModules.set("OnTimer", new Timer<OnTimerDataInterface>("OnTimer"));
timerModules.set("OffTimer", new Timer<OffTimerDataInterface>("OffTimer"));

/**
 * @openapi
 *   /timer/add:
 *      post:
 *        description: Add Timer to Specific Timer Module
 *        parameters:
 *          - in: query
 *            name: name
 *            schema:
 *              type: string
 *          - in: query
 *            name: id
 *            schema:
 *              type: string
 *        responses:
 *          200:
 *            description: Successfully Add Timer
 *
 *
 */
router.post("/add", (req, res) => {
  log.info(req.query);
  res.send("Add Timer RestAPI Called");
});

export default router;

import { Timer } from "../Timer";

export interface OnTimerDataInterface extends TimerIdInterface {
  message: string;
}

export interface OffTimerDataInterface extends TimerIdInterface {
  message: string;
}

export interface TimerIdInterface {
  id: string;
}

export type TimerModulesType = Timer<
  OnTimerDataInterface | OffTimerDataInterface
>;

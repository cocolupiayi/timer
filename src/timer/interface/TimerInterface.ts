export interface TimerInterface<T> {
  /** Unique ID of Timer */
  id: string;
  /** Unique of Time */
  time: Date;
  data: T;
}

export interface TimerModuleInterface<T> {
  list: Map<string, T>;
  name: string;
}

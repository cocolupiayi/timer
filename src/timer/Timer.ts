// import RestrictedAreaInterface from "../restrictedArea/RestrictedAreaInterface";
import { TimerModuleInterface } from "./interface/TimerInterface";

export class Timer<T extends { id: string }>
  implements TimerModuleInterface<T>
{
  public list: Map<string, T>;
  public name: string;

  constructor(name: string) {
    this.list = new Map<string, T>();
    this.name = name;
  }

  addTimer(data: T) {
    this.list.set(data.id, data);
  }

  removeTimer(id: string) {
    this.list.delete(id);
  }

  checkTimerExist(id: string) {
    return this.list.has(id);
  }

  getTimer(id: string) {
    return this.list.get(id);
  }
}
